package br.com.stency.warps.data;

import lombok.Data;
import org.bukkit.Location;

@Data public class Warp {

    private String name;
    private Location location;

    public Warp(String name){
        this.name = name;
    }
}
