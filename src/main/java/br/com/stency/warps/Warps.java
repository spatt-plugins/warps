package br.com.stency.warps;

import br.com.stency.common.helpers.CommonPlugin;
import br.com.stency.warps.command.WarpCommand;
import br.com.stency.warps.command.WarpDeleteCommand;
import br.com.stency.warps.command.WarpSetCommand;
import br.com.stency.warps.service.WarpService;
import br.com.stency.warps.service.impl.WarpServiceImpl;
import lombok.Getter;

public class Warps extends CommonPlugin {

    // Permissão padrão para warps: stency.warps.(nome)

    @Getter
    public static Warps instance;

    public Warps(){
        instance = this;
        provideService(WarpService.class,new WarpServiceImpl());
    }

    @Override
    public void enable() {
        register(this,new WarpCommand(),new WarpSetCommand(),new WarpDeleteCommand());
        getService(WarpService.class).init();

    }

    @Override
    public void disable() {
        getService(WarpService.class).stop();
    }

    @Override
    public void load() {

    }
}
