package br.com.stency.warps.command;

import br.com.stency.common.util.command.CommonCommand;
import br.com.stency.common.util.command.annotation.Command;
import br.com.stency.common.util.command.annotation.Permission;
import br.com.stency.warps.Warps;
import br.com.stency.warps.data.Warp;
import br.com.stency.warps.service.WarpService;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpSetCommand extends CommonCommand {


    @Command("setwarp")
    @Permission("stency.commands.setwarp")
    @Override
    public void command(CommandSender commandSender, Player player, String[] arguments) {
        if (arguments.length == 0){
            player.sendMessage("§cUse /setwarp (nome).");
        }else{
            Warp warp = Warps.getInstance().getService(WarpService.class).get(arguments[0]);
            if (warp != null){
                player.sendMessage("§cJá existe uma warp com este nome.");
            }else{
                warp = Warps.getInstance().getService(WarpService.class).create(arguments[0]);
                warp.setLocation(player.getLocation().getBlock().getLocation().add(0.5,0,0.5));
                warp.getLocation().setDirection(player.getLocation().getDirection());
                warp.getLocation().setPitch(player.getLocation().getPitch());
                warp.getLocation().setYaw(player.getLocation().getYaw());
                player.sendMessage("§aVocê criou a warp §f'" + warp.getName() + "'§a com sucesso.");
            }
        }
    }
}
