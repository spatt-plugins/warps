package br.com.stency.warps.command;

import br.com.stency.common.util.command.CommonCommand;
import br.com.stency.common.util.command.annotation.Command;
import br.com.stency.common.util.command.annotation.Permission;
import br.com.stency.warps.Warps;
import br.com.stency.warps.data.Warp;
import br.com.stency.warps.service.WarpService;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpDeleteCommand extends CommonCommand {

    @Command("delwarp")
    @Permission("stency.commands.delwarp")
    @Override
    public void command(CommandSender commandSender, Player player, String[] arguments) {
        if (arguments.length == 0){
            player.sendMessage("§cUse /delwarp (nome).");
        }else{
            Warp warp = Warps.getInstance().getService(WarpService.class).get(arguments[0]);
            if (warp == null){
                player.sendMessage("§cNão existe nenhuma warp com este nome.");
            }else{
                Warps.getInstance().getService(WarpService.class).delete(warp.getName());
                Warps.getInstance().getService(WarpService.class).config().set("Warps." + warp.getName(),null);
                Warps.getInstance().getService(WarpService.class).config().save();
                player.sendMessage("§aVocê deletou a warp §f'" + warp.getName() + "'§a com sucesso.");
            }
        }
    }
}
