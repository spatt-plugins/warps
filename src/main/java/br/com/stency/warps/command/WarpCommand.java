package br.com.stency.warps.command;

import br.com.stency.common.util.command.CommonCommand;
import br.com.stency.common.util.command.annotation.Command;
import br.com.stency.common.util.command.annotation.Permission;
import br.com.stency.warps.Warps;
import br.com.stency.warps.data.Warp;
import br.com.stency.warps.service.WarpService;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpCommand extends CommonCommand {


    @Command("warp")
    @Permission("stency.commands.warp")
    @Override
    public void command(CommandSender commandSender, Player player, String[] arguments) {
        if (arguments.length == 0){
            player.sendMessage("§cUse /warp (warp).");
        }else{
            Warp warp = Warps.getInstance().getService(WarpService.class).get(arguments[0]);
            if (warp == null){
                player.sendMessage("§cA warp §f'" + arguments[0] + "'§c não existe.");
            }else{
                if (player.hasPermission("stency.warps." +warp.getName().toLowerCase())){
                    System.out.println(warp.getLocation());
                    System.out.println(warp.getName());
                    player.teleport(warp.getLocation());
                    player.sendMessage("§aVocê foi teleportado(a) até a warp §f'" + warp.getName() + "'§a.");
                }else{
                    player.sendMessage("§cVocê não tem permissão para se teleportar até essa warp.");
                }
            }
        }
    }
}
