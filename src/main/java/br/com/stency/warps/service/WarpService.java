package br.com.stency.warps.service;

import br.com.stency.common.util.file.CommonConfig;
import br.com.stency.warps.data.Warp;

import java.util.List;

public interface WarpService {

    Warp get(String name);
    void delete(String name);
    Warp create(String name);
    Warp add(String name);

    List<Warp> all();
    CommonConfig config();

    void init();
    void stop();
}
