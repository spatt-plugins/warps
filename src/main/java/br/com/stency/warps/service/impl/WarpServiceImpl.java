package br.com.stency.warps.service.impl;

import br.com.stency.common.util.file.CommonConfig;
import br.com.stency.warps.Warps;
import br.com.stency.warps.data.Warp;
import br.com.stency.warps.service.WarpService;
import com.google.common.collect.Maps;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WarpServiceImpl implements WarpService {

    private Map<String,Warp> warpMap = Maps.newHashMap();
    private CommonConfig config = new CommonConfig(Warps.getInstance(),"warps.yml");

    @Override
    public Warp get(String name) {
        return warpMap.get(name);
    }

    @Override
    public void delete(String name) {
        warpMap.remove(name);
    }

    @Override
    public Warp create(String name) {
        if (get(name) == null){
            return add(name);
        }
        return get(name);
    }

    @Override
    public Warp add(String name) {
        Warp warp = new Warp(name);
        warpMap.put(name,warp);
        return warp;
    }

    @Override
    public List<Warp> all() {
        return new ArrayList<>(warpMap.values());
    }

    @Override
    public CommonConfig config() {
        return config;
    }

    @Override
    public void init() {
        ConfigurationSection section = config.getConfigurationSection("Warps");
        if (section == null)return;
        for (String value : section.getKeys(false)){
            Warp warp = add(value);
            warp.setLocation(config.getLocation("Warps." + value + ".location"));
        }

    }

    @Override
    public void stop() {
        for (Warp warp : all()){
            config.set("Warps." + warp.getName() + ".location",warp.getLocation());
        }
        config.save();
    }
}
